import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Service, ServiceRelations, Employe} from '../models';
import {EmployeRepository} from './employe.repository';

export class ServiceRepository extends DefaultCrudRepository<
  Service,
  typeof Service.prototype.id,
  ServiceRelations
> {

  public readonly employes: HasManyRepositoryFactory<Employe, typeof Service.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('EmployeRepository') protected employeRepositoryGetter: Getter<EmployeRepository>,
  ) {
    super(Service, dataSource);
    this.employes = this.createHasManyRepositoryFactoryFor('employes', employeRepositoryGetter,);
    this.registerInclusionResolver('employes', this.employes.inclusionResolver);
  }
}
