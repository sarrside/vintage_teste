import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Employe, EmployeRelations} from '../models';

export class EmployeRepository extends DefaultCrudRepository<
  Employe,
  typeof Employe.prototype.id,
  EmployeRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Employe, dataSource);
  }
}
