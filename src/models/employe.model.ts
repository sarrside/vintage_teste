import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Employe extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  nom?: string;

  @property({
    type: 'string',
  })
  prenom?: string;

  @property({
    type: 'date',
  })
  dateDeNaiss?: string;

  @property({
    type: 'string',
  })
  serviceId?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Employe>) {
    super(data);
  }
}

export interface EmployeRelations {
  // describe navigational properties here
}

export type EmployeWithRelations = Employe & EmployeRelations;
