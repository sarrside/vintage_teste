import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Service,
  Employe,
} from '../models';
import {ServiceRepository} from '../repositories';

export class ServiceEmployeController {
  constructor(
    @repository(ServiceRepository) protected serviceRepository: ServiceRepository,
  ) { }

  @get('/services/{id}/employes', {
    responses: {
      '200': {
        description: 'Array of Service has many Employe',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Employe)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Employe>,
  ): Promise<Employe[]> {
    return this.serviceRepository.employes(id).find(filter);
  }

  @post('/services/{id}/employes', {
    responses: {
      '200': {
        description: 'Service model instance',
        content: {'application/json': {schema: getModelSchemaRef(Employe)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Service.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Employe, {
            title: 'NewEmployeInService',
            exclude: ['id'],
            optional: ['serviceId']
          }),
        },
      },
    }) employe: Omit<Employe, 'id'>,
  ): Promise<Employe> {
    return this.serviceRepository.employes(id).create(employe);
  }

  @patch('/services/{id}/employes', {
    responses: {
      '200': {
        description: 'Service.Employe PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Employe, {partial: true}),
        },
      },
    })
    employe: Partial<Employe>,
    @param.query.object('where', getWhereSchemaFor(Employe)) where?: Where<Employe>,
  ): Promise<Count> {
    return this.serviceRepository.employes(id).patch(employe, where);
  }

  @del('/services/{id}/employes', {
    responses: {
      '200': {
        description: 'Service.Employe DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Employe)) where?: Where<Employe>,
  ): Promise<Count> {
    return this.serviceRepository.employes(id).delete(where);
  }
}
